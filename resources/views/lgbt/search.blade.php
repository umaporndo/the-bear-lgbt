@extends('layouts.app')

@section('page-title', __('lgbt.page_title.index'))
@section('page-description', __('lgbt.page_description.index'))
@section('page-keyword', __('lgbt.page_keyword.index'))

@section('og-image', asset(config('images.open_graph.default_image')))
@section('og-title', __('lgbt.og_title.index'))
@section('og-description', __('lgbt.og_description.index'))
@section('og-keyword', __('lgbt.og_keyword.index'))
@section('og-url', __('lgbt.og_url.index') )

@section('content')
    <div class="content-desktop">
    </div>
    <div>
        <div class="container content-detail search-content pt-2" >
            <form id="search-form" class="search-box" method="GET" action="{{ route('lgbt.search') }}">
                {{ csrf_field() }}
                <div class="p-3 row">
                    <div class="col-8 p-0">
                        <input type="text" class="form-control" name="search" id="search" placeholder="e.g. city, places, hotel…">
                    </div>
                    <div class="col-4 pl-1">
                        <button class="button-green pointer" type="submit">
                            Search
                        </button>
                    </div>
                </div>
            </form>
            <h1>Result</h1>
            <div id="content-list-box">
                @include('lgbt.search_list')
            </div>
        </div>
    </div>
@endsection
