<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Travel Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index'  => 'The bear lgbt',
        'detail' => 'The bear lgbt',
    ],

    'page_title' => [
        'index'  => 'The bear lgbt',
        'detail' => ':lgbt_title | The bear lgbt',
        'menu'   => 'The bear lgbt | :menu_title',
    ],

    'page_description' => [
        'index'  => 'The bear lgbt',
        'detail' => 'The bear lgbt :lgbt_description',
        'menu'   => 'The bear lgbt | :menu_title',
    ],

    'page_keyword' => [
        'index'  => 'The bear lgbt',
        'detail' => 'The bear lgbt :lgbt_keyword',
        'menu'   => 'The bear lgbt | :menu_title',
    ],

    'og_title' => [
        'index'  => 'The bear lgbt',
        'detail' => ':lgbt_title | The bear lgbt',
        'menu'   => 'The bear lgbt | :menu_title',
    ],

    'og_description' => [
        'index'  => 'The bear lgbt',
        'detail' => ':lgbt_description',
        'menu'   => 'The bear lgbt | :menu_title',
    ],

    'og_keyword' => [
        'index'  => 'The bear lgbt',
        'detail' => ':lgbt_keyword',
        'menu'   => 'The bear lgbt | :menu_title',
    ],

    'og_url' => [
        'index'  => route( 'lgbt.index' ),
        'detail' => route( 'lgbt.detail', [ 'slug' => ':slug', 'id' => ':lgbt_id' ] ),
        'menu'   => route( 'lgbt.menu', [ 'slug' => ':slug', 'menuID' => ':menu_id' ] ),
    ],

];
